<?
/*
Plugin Name: Calk plugin
Description: Калькулятор кондиционеров.
Author: Petr
Version: 1.0
Author URI: none
*/

/*наш шорткод посмотри в интеренете как выводить через шорткод*/
function calk_shortcode_list($atts = null)
{
    ?>
    <div class="inc_calck">
        <p class="calk-title"><b>Для подбора сплит-системы для вашего помещения заполните форму:</b></p>
        <div class="calck_content">
            <form id="calck_form">
                <input type="hidden" name="way" value="<?= admin_url('admin-ajax.php') ?>">
                <div class="option_room">
                    <label class="flat-wrap">
                        <span class="left-area">Площадь помещения</span>
                        <div class="center-area">
                            <input type="text" name="area" required id="flat-area">
                            <span class="units"> кв. м. </span>
                        </div>
                        <div id="flat-slider"></div>
                    </label>
                    <label class="ceiling-wrap">
                        <span class="left-area">Высота потолка </span>
                        <div class="center-area">
                            <input type="text" name="ceiling" required id="ceiling-height">
                            <span class="units"> м. </span>
                        </div>
                        <span class="ceiling-area"></span>
                        <div id="ceiling-slider"></div>
                    </label>
                </div>

                <div class="price_dial">

                    <div class="price-wrap">
                      <span>От:</span> <input type="text" class="amount-min" id="amount" name="price_start" required>
                      <span>До:</span> <input type="text" class="amount-max" id="amount" name="price_end" required>
                    </div>
                    

                    <div class="range-wrap">
                      <p class="range-text">Введите ценовой диапазон, руб</p>
                      <div class="slider-range"></div>
                    </div>
                    

                </div>
        
                <div class="people">
                    <p>Количество человек</p>
                    
                    <div class="people-wrap">
                      
                          <input type="radio" name="people" value="0.1" id="one-people">
                          <label for="one-people"></label>
                          <img src="http://palazar.ru/wp-content/uploads/2017/07/1.png">
        
                      
                          <input type="radio" name="people" value="0.2" id="two-people">
                          <label for="two-people"></label>
                          <img src="http://palazar.ru/wp-content/uploads/2017/07/2.png">
                      

                      
                          <input type="radio" name="people" value="0.3" id="four-people">
                          <label for="four-people"></label>
                          <img src="http://palazar.ru/wp-content/uploads/2017/07/4.png">
                      
                    </div>
                </div>

                <div class="tehno">
                    <p>Какая техника есть в помещении?</p>
                    
                    <div class="tehno-wrap">
                      <input type="checkbox" name="holod" value="0.2" id="holod">
                      <label for="holod" >Холодильник</label>
                      <input type="checkbox" name="tel" value="0.2" id="tel">
                      <label for="tel">Телевизор</label>
                    </div>
                   

                </div>
                
                <div class="button-wrap">
                  <button type="submit" name="submit" class="button" >Подобрать</button>
                </div>
                
            </form>
            <div class="result_calck_table"></div>
        </div>
    </div>


    <?
}

add_shortcode('calk_shortcode', 'calk_shortcode_list');


/*проверка на фильтр*/
add_filter('the_content', 'filter_calck_content');
function filter_calck_content($content)
{

    if (has_shortcode($content, "calk_shortcode")) {
        wp_enqueue_script('jquery_ui', plugins_url('calk_palazar/js/jquery-ui.min.js'), array('jquery'), null, true);
        wp_enqueue_script('my_cart', plugins_url('calk_palazar/js/calck_script.js'), array('jquery'), null, true);
        wp_enqueue_style('calck', plugins_url('calk_palazar/css/jquery-ui.min.css'), false, '1.1', 'all');
        wp_enqueue_style('calck', plugins_url('calk_palazar/css/calk_style.css'), false, '1.1', 'all');
    }
    return $content;
}

/*проверка на главную страницу*/
function calck_scripts()
{
    if (is_front_page()) {
        //подключаем скрипт
        wp_enqueue_script('jquery_ui', plugins_url('calk_palazar/js/jquery-ui.min.js'), array('jquery'), null, true);
        wp_enqueue_script('my_cart', plugins_url('calk_palazar/js/calck_script.js'), array('jquery'), null, true);
        wp_enqueue_style('jquery_ui-css', plugins_url('calk_palazar/css/jquery-ui.min.css'), false, '1.1', 'all');
        wp_enqueue_style('calck', plugins_url('calk_palazar/css/calk_style.css'), false, '1.1', 'all');
    }
}

add_action('wp_enqueue_scripts', 'calck_scripts');


/*Обработка ajax*/
function azzrael_ajax_post()
{
    /*Сортировка по ценам*/
    $price_start = $_REQUEST['price_start'];
    $price_end = $_REQUEST['price_end'];
    /*Сортировка по площади */
    $area = $_REQUEST['area'];
    $dop_tehno = getArea($_REQUEST['people'], $_REQUEST['holod'], $_REQUEST['tel']);
    $area = (($area * $_REQUEST['ceiling'] * 35) / 1000) + $dop_tehno;
    $area1 = $area - 0.5;
    $area2 = $area + 0.5;
    global $wpdb;
    /*Получаем записи по кв*/
    $newtable = $wpdb->get_results("SELECT * FROM wp_postmeta WHERE (meta_key = 'cooling') AND (meta_value BETWEEN '$area1'  AND  '$area2')  ORDER BY meta_value ASC",
        ARRAY_A);
    /*Получаем записи по цене*/
    $item = '';
    foreach ($newtable as $key => $items) {
        $item .= $items['post_id'] . ', ';
    }
    $item = substr($item, 0, strlen($item) - 2);
    if ($price_start < 10000) {
        $price_item = $wpdb->get_results("SELECT * FROM wp_postmeta WHERE `meta_key` = 'price' AND `meta_value` < $price_end ORDER BY meta_value ASC",
            ARRAY_A);
    } else {
        $price_item = $wpdb->get_results("SELECT * FROM wp_postmeta WHERE meta_key = 'price' AND (meta_value > $price_start  AND  meta_value < $price_end) ORDER BY meta_value ASC",
            ARRAY_A);
    }
    $item2 = '';
    if (!empty($price_item)) {
        foreach ($price_item as $key => $items2) {
            if(stristr($item, $items2['post_id']) != FALSE) {
                $item2 .= $items2['post_id'] . ', ';
            }
        }
        $item2 = substr($item2, 0, strlen($item2) - 2);
    }

    if (empty($item2)) {
        $item2 = $item;
    }

    /*получение записей*/
    $posts = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `ID` IN ($item2) AND (post_status = 'publish') ",
        ARRAY_A);
    /*выводим посты*/
    /*$query = new WP_Query( array(
        'ID' => array($item2),
        'meta_key' => 'price',
        'orderby' => 'meta_value',
        'order' => 'ASC',
    ) );*/

    ?>
    <table>
        <thead>
        <th></th>
        <th>Модель</th>
        <th>Охлаждение, кВт</th>
        <th>Нагрев, кВт</th>
        <th>Розн. цена, Рубли</th>
        </thead>
        <?
        $item2 = explode(",", $item2);
        foreach ($item2 as $key2 => $value) {
            $value = trim($value);
            foreach ($posts as $key => $post) {

                if ($post['ID'] == $value) {

                    /*получаем ссылку на пост*/
                    $go_link = get_permalink($post['ID']);
                    /*Получаем доп поля*/
                    $meta = new stdClass;
                    foreach (get_post_meta($post['ID']) as $k => $v) {
                        $meta->$k = $v[0];
                    }
                    /*Охлаждение*/
                    $cooling = $meta->cooling;
                    /*Нагрев*/
                    $heat = $meta->heat;
                    /*Стоимость*/
                    $price = $meta->price;
                    /*Модель*/
                    $model = $meta->model;
                    ?>

                    <tr>
                        <td class="calck-item-thumbmail">
                            <a href="<?= $go_link ?>">
                                <? echo get_the_post_thumbnail($post['ID'])?>
                            </a>
                        </td>
                        <td><a href="<?= $go_link ?>"><?= $post['post_title'] ?></a></td>
                        <td><?= $cooling ?></td>
                        <td><?= $heat ?></td>
                        <td><?= $price ?></td>
                    </tr>

                    <?
                }
            }
        }
        ?>
    </table>
    <?
    die();
}


function getArea($people = 0, $holod = 0, $tel = 0)
{
    $area = 0;
    $area += ($people + $holod + $tel);
    return $area;
}

add_action('wp_ajax_nopriv_azzrael_ajax_post', 'azzrael_ajax_post');
add_action('wp_ajax_azzrael_ajax_post', 'azzrael_ajax_post');

?>