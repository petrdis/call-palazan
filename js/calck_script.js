$('body').on('submit', '#calck_form', function(){
    var data = $(this).serialize();
    way = $(this).find('input[name="way"]').val();
    $.ajax({
        type: "POST",
        url: way,
        data: "action=azzrael_ajax_post&"+data,
        success: function(html) {
            // console.log(html);
            $('.result_calck_table').empty();
            $('.result_calck_table').append(html);
        }
    });

    return false;
})



$(function () {
    $(".slider-range").slider({
        range: true,
        min: 8000,
        max: 150000,
        values: [15000, 30000],
        slide: function (event, ui) {
            $(".amount-min").val(ui.values[0]);
            $(".amount-max").val(ui.values[1]);
        }
    });
    $(".amount-min").val($('.slider-range').eq(0).slider("values", 0));
    $(".amount-max").val($('.slider-range').eq(0).slider("values", 1));

    $('.amount-min').on('change', function () {
        $('.slider-range').eq(0).slider("values", 0, $(this).val());
    });

    $('.amount-max').on('change', function () {
        $('.slider-range').eq(0).slider("values", 1, $(this).val());
    });
});


$(function () {
    $("#flat-slider").slider({
        range: "min",
        value: 20,
        min: 1,
        max: 100,
        slide: function (event, ui) {
            $("#flat-area").val(ui.value);
        }
    });
    $("#flat-area").val($("#flat-slider").slider("value"));

    $('#flat-area').change(function () {
        $("#flat-slider").slider("value", $(this).val());
    });


    $("#ceiling-slider").slider({
        range: "min",
        step : 0.1,
        value: 2.5,
        min: 1,
        max: 10,
        slide: function (event, ui) {
            $("#ceiling-height").val(ui.value);
        }
    });
    $("#ceiling-height").val($("#ceiling-slider").slider("value"));

    $('#ceiling-height').change(function () {
        $("#ceiling-slider").slider("value", $(this).val());
    });
});